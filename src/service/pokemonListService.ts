import { IPokemon, IPokemonList, IPokemonListService } from '@/model/model'
import { store } from '@/store/store'
import { api } from '@/transport/pokemonAPI'

class PokemonListService implements IPokemonListService {
	async getPokemonList(pageNumber: number): Promise<IPokemonList> {
		let pokemonList: IPokemonList = {}
		if (store.getPageFromStore(pageNumber)) {
			pokemonList = store.getPageFromStore(pageNumber)
		} else {
			let page: IPokemon[] = await api.fetchPokemonList(
				12,
				(pageNumber - 1) * 12
			)
			page.forEach((pokemon: IPokemon) => {
				pokemonList[pokemon.id] = pokemon
			})
			store.pushPageToStore(pokemonList, pageNumber)
		}
		return pokemonList
	}
	catchPokemon(cardButton: Element) {
		cardButton.addEventListener('click', () => {
			// if (store.pages[pageNumber][id].catch === false) {
			// 	store.pages[pageNumber][id].catch = true
			// 	renderCardList(pokemonList).then((list: Element) => {
			// 		ul.replaceWith(list)
			// 	})
			//}
		})
	}
}

export const pokemonListService = new PokemonListService()
