import { createRoot } from 'react-dom/client'
import { App } from './view/App'
import './view/app.scss'

const root = document.getElementById('root')

const container = createRoot(root)

container.render(<App />)
