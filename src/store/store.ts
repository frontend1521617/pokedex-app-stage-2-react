import { IPages, IPokemonList, IStore } from '@/model/model'
class Store implements IStore {
	pages: IPages = {}
	currentPage: number = 1
	caughtPokemons: []

	getPageFromStore(pageNumber: number) {
		return this.pages[pageNumber]
	}
	pushPageToStore(pokemonList: IPokemonList, pageNumber: number) {
		this.pages[pageNumber] = pokemonList
	}

	setCurrentPage(pageNumber: number) {
		this.currentPage = pageNumber
	}

	getCaughtPokemons() {
		return this.caughtPokemons
	}
}
export const store = new Store()
