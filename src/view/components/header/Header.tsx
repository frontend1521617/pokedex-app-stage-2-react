import { Link } from 'react-router-dom'

export const Header = () => {
	return (
		<header>
			<img src='./logo.png' alt='logo'></img>
			<nav>
				<Link to='/home'>Home</Link>
				<Link to='/caught'>Caught pokemons</Link>
			</nav>
		</header>
	)
}
