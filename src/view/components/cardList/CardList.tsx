import { IPokemonList } from '@/model/model'
import { FC } from 'react'
import { Card } from '../card/Card'
import styles from './cardList.module.scss'

interface IPokemonListProps {
	pokemonList: IPokemonList
}

export const CardList: FC<IPokemonListProps> = ({ pokemonList }) => {
	return (
		<ul className={styles.cardList}>
			{Object.values(pokemonList).map(pokemon => {
				return <Card key={pokemon.id} pokemon={pokemon} />
			})}
		</ul>
	)
}
