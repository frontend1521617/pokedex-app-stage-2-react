import { IPokemon } from '@/model/model'
import { FC } from 'react'
import styles from './card.module.scss'

interface ICardProps {
	pokemon: IPokemon
}
export const Card: FC<ICardProps> = ({ pokemon }) => {
	return (
		<li className={styles.card}>
			<h2>
				id: {pokemon.id} {pokemon.name}
			</h2>
			<img src={pokemon.avatar} alt='${pokemon.name}' />
			<button className={styles.btn}> Catch me! </button>
		</li>
	)
}
