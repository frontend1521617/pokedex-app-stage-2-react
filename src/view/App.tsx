import { BrowserRouter, Route, Routes } from 'react-router-dom'

import { CaughtPokemons } from './pages/caughtPokemons/CaughtPokemons'
import { Home } from './pages/home/Home'
import { NotFoundPage } from './pages/notFoundPage/NotFoundPage'
import { Pokedex } from './pages/pokedex/Pokedex'

export const App = () => {
	return (
		<BrowserRouter>
			<Routes>
				<Route path='/' element={<Pokedex />}>
					<Route index element={<Home />} />
					<Route path='home' element={<Home />} />
					<Route path='caught' element={<CaughtPokemons />} />
				</Route>
				<Route path='*' element={<NotFoundPage />}></Route>
			</Routes>
		</BrowserRouter>
	)
}
