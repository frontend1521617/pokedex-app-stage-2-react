import { IPokemonList } from '@/model/model'
import { pokemonListService } from '@/service/pokemonListService'
import { CardList } from '@/view/components/cardList/CardList'
import { Pagination } from '@/view/components/pagination/Pagination'
import { useEffect, useState } from 'react'

export const Home = () => {
	const [pokemonList, setPokemonList] = useState<IPokemonList>({})
	const [pageNumber, setPageNumber] = useState<number>(1)
	useEffect(() => {
		pokemonListService.getPokemonList(1).then(list => {
			setPokemonList(list)
		})
	})
	return (
		<main>
			<CardList pokemonList={pokemonList} />
			<Pagination />
		</main>
	)
}
