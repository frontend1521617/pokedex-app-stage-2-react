import { Header } from '@/view/components/header/Header'
import { Outlet } from 'react-router-dom'

export const Pokedex = () => {
	return (
		<>
			<Header />
			<Outlet />
		</>
	)
}
