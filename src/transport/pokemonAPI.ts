import { IPokemon, IPokemonAPI, IPokemonListItem } from '@/model/model'

class PokemonAPI implements IPokemonAPI {
	baseURL = 'https://pokeapi.co/api/v2/pokemon/'

	constructor(public query = '?limit=20&offset=0') {}

	async fetchPokemon(item: IPokemonListItem): Promise<IPokemon> {
		let response = await fetch(item.url)
		let pokemon = await response.json()
		return {
			id: pokemon.id,
			name: pokemon.name,
			avatar: pokemon.sprites.front_default,
		}
	}

	async fetchPokemonList(limit: number, offset: number): Promise<IPokemon[]> {
		let response = await fetch(
			`${this.baseURL}?limit=${limit}&offset=${offset}`
		)
		let page = await response.json()
		let pokemonList: IPokemon[] = await Promise.all(
			page.results.map(this.fetchPokemon)
		)
		return pokemonList
	}
}

export const api = new PokemonAPI()
