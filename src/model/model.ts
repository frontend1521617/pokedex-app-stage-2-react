export interface IPokemon {
	id: number
	name: string
	avatar: string
}
export interface IPokemonList {
	[id: number]: IPokemon
}
export interface IPokemonListItem {
	name: string
	url: string
}

export interface IPages {
	[key: number]: IPokemonList
}

export interface IPokemonAPI {
	baseURL: string
	fetchPokemon(pokemonListItem: IPokemonListItem): Promise<IPokemon>
	fetchPokemonList(limit: number, offset: number): Promise<IPokemon[]>
}

export interface IStore {
	pages: IPages
	currentPage: number
	caughtPokemons: IPokemon[]
	getPageFromStore(pageNumber: number): IPokemonList
	pushPageToStore(pokemonList: IPokemonList, pageNumber: number): void
	setCurrentPage(pageNumber: number): void
	getCaughtPokemons(): IPokemon[]
}

export interface IPokemonListService {
	getPokemonList(pageNumber: number): Promise<IPokemonList>
	catchPokemon(cardButton: Element): void
}
